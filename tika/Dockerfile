FROM ubuntu:bionic as base
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get -y upgrade

LABEL maintainer="Laurent Cherpit <laurent@ttree.ch>"

FROM base as dependencies

ENV TIKA_VERSION="1.25" \
    TIKA_HOME="/opt/tika"

# -e Exit immediately if a command exits with a non-zero status.
# -x Print commands and their arguments as they are executed
RUN set -ex; \
    export DEBIAN_FRONTEND=noninteractive; \
    apt-get -y install openjdk-11-jre-headless gdal-bin tesseract-ocr tesseract-ocr-fra

RUN set -ex; \
    echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
    && export DEBIAN_FRONTEND=noninteractive; \
    apt-get install -y xfonts-utils fonts-freefont-ttf fonts-liberation ttf-mscorefonts-installer wget cabextract

FROM dependencies as fetch_tika

ENV NEAREST_TIKA_SERVER_URL="https://www.apache.org/dyn/closer.cgi/tika/tika-server-${TIKA_VERSION}.jar?filename=tika/tika-server-${TIKA_VERSION}.jar&action=download" \
    ARCHIVE_TIKA_SERVER_URL="https://archive.apache.org/dist/tika/tika-server-${TIKA_VERSION}.jar" \
    DEFAULT_TIKA_SERVER_ASC_URL="https://www.apache.org/dist/tika/tika-server-${TIKA_VERSION}.jar.asc" \
    ARCHIVE_TIKA_SERVER_ASC_URL="https://archive.apache.org/dist/tika/tika-server-${TIKA_VERSION}.jar.asc" \
    TIKA_VERSION=$TIKA_VERSION

RUN set -ex; \
    export DEBIAN_FRONTEND=noninteractive; \
    apt-get -y install gnupg2 \
    && wget -t 10 --max-redirect 1 --retry-connrefused -qO- https://www.apache.org/dist/tika/KEYS | gpg --import \
    && wget -t 10 --max-redirect 1 --retry-connrefused $NEAREST_TIKA_SERVER_URL -O /tika-server-${TIKA_VERSION}.jar || rm /tika-server-${TIKA_VERSION}.jar \
    && sh -c "[ -f /tika-server-${TIKA_VERSION}.jar ]" || wget $ARCHIVE_TIKA_SERVER_URL -O /tika-server-${TIKA_VERSION}.jar || rm /tika-server-${TIKA_VERSION}.jar \
    && sh -c "[ -f /tika-server-${TIKA_VERSION}.jar ]" || exit 1 \
    && wget -t 10 --max-redirect 1 --retry-connrefused $DEFAULT_TIKA_SERVER_ASC_URL -O /tika-server-${TIKA_VERSION}.jar.asc  || rm /tika-server-${TIKA_VERSION}.jar.asc \
    && sh -c "[ -f /tika-server-${TIKA_VERSION}.jar.asc ]" || wget $ARCHIVE_TIKA_SERVER_ASC_URL -O /tika-server-${TIKA_VERSION}.jar.asc || rm /tika-server-${TIKA_VERSION}.jar.asc \
    && sh -c "[ -f /tika-server-${TIKA_VERSION}.jar.asc ]" || exit 1 \
    && gpg --verify /tika-server-${TIKA_VERSION}.jar.asc /tika-server-${TIKA_VERSION}.jar

FROM dependencies as runtime
RUN set -ex; \
    apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN set -ex; \
     mkdir -p "${TIKA_HOME}/bin" "${TIKA_HOME}/scripts";

ARG BUILD_DATE
# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.license="MIT" \
      org.label-schema.name="${COMPOSE_PROJECT_NAME} - tika base ubuntu bionic" \
      org.label-schema.url="https://gitlab.ttree.ch/tc/ceg-echo.git" \
      org.label-schema.vcs-url="https://bitbucket.org/techniconcept/echo_docker.git" \
      org.label-schema.vcs-type="Git"

ENV TIKA_VERSION=$TIKA_VERSION \
    TIKA_USER="tika" \
    TIKA_UID="9998" \
    TIKA_GROUP="tika" \
    TIKA_GID="9998" \
    PATH="${TIKA_HOME}/bin:${TIKA_HOME}/scripts:$PATH"

RUN set -ex; \
    groupadd -r --gid "$TIKA_GID" "$TIKA_GROUP"; \
    useradd -r --uid "$TIKA_UID" --gid "$TIKA_GID" "$TIKA_USER"

RUN set -ex; \
    chmod -R 0755 "${TIKA_HOME}/bin" "${TIKA_HOME}/scripts"; \
    chown -R "${TIKA_USER}:${TIKA_GROUP}" ${TIKA_HOME}

COPY --from=fetch_tika /tika-server-${TIKA_VERSION}.jar ${TIKA_HOME}/bin/tika-server-${TIKA_VERSION}.jar

COPY ./tika-config.xml ${TIKA_HOME}/tika-config.xml

RUN set -ex; \
    chmod 0440 ${TIKA_HOME}/tika-config.xml; \
    chown "${TIKA_USER}:${TIKA_GROUP}" ${TIKA_HOME}/tika-config.xml

EXPOSE 9998
WORKDIR ${TIKA_HOME}
USER $TIKA_USER

ENTRYPOINT echo "Java version:" \
    && java -version \
    && echo "Tesseract version:" \
    && tesseract -v \
    && echo "Tika version: ${TIKA_VERSION}" \
    && echo "Configuration:" \
    && cat ./tika-config.xml \
    && java -cp "./bin/tika-server-${TIKA_VERSION}.jar" org.apache.tika.server.TikaServerCli --h 0.0.0.0 --port 9998 --config ./tika-config.xml