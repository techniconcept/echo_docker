include ./Makefile.base

build-all:  ##@base build all images
	$(DOCKER_COMPOSE) build --pull --build-arg BUILD_DATE=$(DATE_NOW)

build-tika: ##@base build tika image
	$(DOCKER_COMPOSE) build --pull --build-arg BUILD_DATE=$(DATE_NOW) tika

build-solr: ##@base build solr image
	$(DOCKER_COMPOSE) build --pull --build-arg BUILD_DATE=$(DATE_NOW) solr


up-dev:     ##@run up -d --no-build : start or re-create container. Local env-dev stack (may require run `make pull`)
	$(DOCKER_COMPOSE) -f docker-compose.yaml -f docker-compose.dev.yaml up -d --no-build

up:         ##@run start or re-create container. Prod env stack
	$(DOCKER_COMPOSE) -f docker-compose.yaml up -d --no-build

pull:       ##@base get image from registry server. require: docker login registry.server.name

stop:         ##@run stop
	$(DOCKER_COMPOSE) stop

down:         ##@run down
	$(DOCKER_COMPOSE) down --remove-orphans

config-dev: ##@show compose config prod env stack
	$(DOCKER_COMPOSE) -f docker-compose.yaml -f docker-compose.dev.yaml config

config:     ##@show compose config prod env stack
	$(DOCKER_COMPOSE) -f docker-compose.yaml config
