FROM alpine:latest AS baselayout

RUN apk add --no-cache \
        ca-certificates \
        openssl \
        curl \
        bash \
        sed \
        wget \
        zip \
        unzip \
        bzip2 \
        p7zip \
        drill \
        ldns \
        openssh-client \
        rsync \
        git \
        gnupg \
    ## Install go-replace
    && wget -O "/usr/local/bin/go-replace" "https://github.com/webdevops/goreplace/releases/download/1.1.2/gr-64-linux" \
    && chmod +x "/usr/local/bin/go-replace" \
    && "/usr/local/bin/go-replace" --version

RUN mkdir -p \
        /baselayout/sbin \
        /baselayout/usr/local/bin \
    # Baselayout scripts
    && wget -O /tmp/baselayout-install.sh https://raw.githubusercontent.com/webdevops/Docker-Image-Baselayout/master/install.sh \
    && sh /tmp/baselayout-install.sh /baselayout \
    ## Install go-replace
    && wget -O "/baselayout/usr/local/bin/go-replace" "https://github.com/webdevops/goreplace/releases/download/1.1.2/gr-64-linux" \
    && chmod +x "/baselayout/usr/local/bin/go-replace" \
    && "/baselayout/usr/local/bin/go-replace" --version \
    # Install gosu
    && wget -O "/baselayout/sbin/gosu" "https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64" \
    && wget -O "/tmp/gosu.asc" "https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    && gpg --batch --verify /tmp/gosu.asc "/baselayout/sbin/gosu" \
    && rm -rf "$GNUPGHOME" /tmp/gosu.asc \
    && chmod +x "/baselayout/sbin/gosu" \
    && "/baselayout/sbin/gosu" nobody true

####
FROM solr:7.7.3
MAINTAINER Laurent Cherpit <lcherpit@ttree.ch>

ENV TERM="xterm" \
    LANG="C.UTF-8" \
    LC_ALL="C.UTF-8"

USER root
ARG BUILD_DATE

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.license="MIT" \
      org.label-schema.name="${COMPOSE_PROJECT_NAME} - solr base alpine" \
      org.label-schema.url="https://gitlab.ttree.ch/tc/ceg-echo.git" \
      org.label-schema.vcs-url="https://bitbucket.org/techniconcept/echo_docker.git" \
      org.label-schema.vcs-type="Git"

# Baselayout for toolbox !
COPY --from=baselayout /baselayout /

COPY typo3-solr-ext-9/ /tmp/solr-config/

RUN DEBIAN_FRONTEND=noninteractive apt-update \
    && apt-upgrade \
    && apt-install net-tools vim \
    && usermod -u 1000 solr \
    && generate-dockerimage-info \
    && mkdir /tmp/solr \
    && rm -rf /opt/solr/server/solr \
    && mv /tmp/solr-config/ /opt/solr/server/solr \
    && mkdir -p /opt/solr/server/solr/data \
    && chown -R solr:solr /opt/solr \
    && chmod 755 /opt/solr/server/solr \
    && rm -rf /tmp/solr /tmp/solr-config \
    && docker-image-cleanup

USER solr

# volumes defined in docker-compose
