# README #

#### Environnement Docker avec Apache Solr and Apache Tika.
* typo3-solr 9.0
* typo3-tika 4.0
```composer 
    "apache-solr-for-typo3/solr": "^9.0",
    "apache-solr-for-typo3/tika": "^4.0"
```

### À quoi sert ce dépôt ? ###

* Permettre de créer un environment de travail local normalisé avec Docker.
* apache-solr écoute à l'adresse:
  http://localhost:8983/solr (admin console)
  http://localhost:8983/solr/core_fr/select?q=*:*
  
* apache-tika écoute à l'adresse:
  http://localhost:9998 (tika server api)
 

### Installation

#### Docker desktop: 
   
 - S'assurer de ne pas avoir de conflit de variables d'environment.
Nettoyer les variables de `docker-machine` [voir: la cohabitaton avec docker-toolbox : ](https://docs.docker.com/docker-for-mac/docker-toolbox/#setting-up-to-run-docker-desktop-on-mac )

```bash
for env_varname in DOCKER_HOST DOCKER_CERT_PATH DOCKER_TLS_VERIFY DOCKER_MACHINE_NAME
do
unset ${env_varname};
done
```

- Installation avec hombrew cask: 
```bash
brew cask install docker
```
- ou [avec la dmg : ](https://hub.docker.com/editions/community/docker-ce-desktop-mac/)

#### Générer les images et lancer les containers:

Depuis la racine de la copie locale du dépôt. 
Toutes les commandes suivantes sont lancée dans ce dossier, au même niveau que le fichier `Makefile`

**Executer simplement la commande make pour avoir de l'aide**
```bash
make
```
**Créer ou générer les images**
```bash
 make build-all
```
ou seulement un services
```bash
 make build-tika
ou alors:
 make build-solr
```
Il y a deux environnement. _(pour l'instant pas beaucoup de différences. si ce n'est le volumes pour les indexes)_

**Pour démarer les services**

- En local:
```bash
make up-dev
```
- Pour la prod:
```bash
make up
```

**Docker desktop Dashboard**

![Docker desktop Dashboard](./Documentation/Images/docker-dahsboard.png)

Pour arrêter et démarrer les services: _(peut aussi être effecté depuis le Dashboard de Docker.app)_
```bash
docker-compose stop
docker-compose start
```
Pour supprimer les containers avec les services: `docker-compose down`
_Les données des indexes sont conservés localement dans le dossier `./data-solr`_
```bash
docker-compose down 
Stopping echo-solr ... done
Stopping echo-tika ... done
Removing echo-solr ... done
Removing echo-tika ... done
Removing network echo_search_default
```

#### Autres commandes utiles
Lister les containers ou les processus:
```bash
docker-compose ps
docker-compose top
```

Lister les images locales
```bash
docker image ls
```

Lister les infos sur les containers:
```bash
docker container ls
```

Ouvrir un process bash dans un container avc son nom ou son container id.
```bash
docker exec -it echo-solr /bin/bash
```

### Related documentation
* [Tika Tesseract OCR Parser](https://cwiki.apache.org/confluence/display/TIKA/TikaOCR)

----
### todo: ###
* add cert and ssl 

### Déploiement d'un nouveau core sur VDN147
#### Préparation du nouvel image
Préparer la configuration dans solr/typo3-solr-ext-9/cores
```bash
docker-compose build
docker-compose stop
docker-compose down
docker-compose up -d
```
Le nouveau core doit être disponible en local

#### Importer la nouvelle image sur VDN147
En local
```bash
docker save registry.ttree.ch/ceg_echo/solr:7.6 | gzip > solr.tar.gz
scp solr.tar.gz webceg@vdn147.vdn.ne.ch:/tmp/
```

Sur VDN147

**SOLR**
```bash
sudo su
docker load < /tmp/solr.tar.gz
docker stop solr
docker rm solr
docker container run -e LOG4J_FORMAT_MSG_NO_LOOKUPS=true --mount type=bind,source=/mnt/solr_data/echo,target=/opt/solr/server/solr/data --restart unless-stopped -p 8983:8983 --name solr -d registry.ttree.ch/ceg_echo/solr:7.6
docker image prune
rm /tmp/solr.tar.gz
```

**Tika**
```bash
sudo su
docker load < /tmp/tika.tar.gz
docker stop tika
docker rm tika
docker container run --restart unless-stopped -p 9998:9998 --name tika -d registry.ttree.ch/ceg_echo/tika:1.23
docker image prune
rm /tmp/tika.tar.gz
```
